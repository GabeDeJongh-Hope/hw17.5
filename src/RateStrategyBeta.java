package src;

public class RateStrategyBeta implements RateStrategy {
	/**
	 * This class is meant to be simple, its only purpose is testing for homework assignment
	 * 17.5 from the book. 1 cent is equal to one minute of time.
	 */
	@Override
	public int computeRate(int amount) {
		return amount;
	}

}
