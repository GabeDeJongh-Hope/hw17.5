package src;

/**
 * Implementation of the pay station.
 * 
 * Responsibilities:
 * 
 * 1) Accept payment; 2) Calculate parking time based on payment; 3) Know
 * earning, parking time bought; 4) Issue receipts; 5) Handle buy and cancel
 * events.
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Computer Science Department Aarhus University
 * 
 * This source code is provided WITHOUT ANY WARRANTY either expressed or
 * implied. You may study, use, modify, and distribute it for non-commercial
 * purposes. For any commercial use, see http://www.baerbak.com/
 */

public class PayStationImpl implements PayStation {
	private int insertedSoFar;
	private int timeBought;
	private RateStrategy rateStrat;
	private CurrencyType currency;

	public PayStationImpl(RateStrategy rateStrat, CurrencyType currency) {
		this.rateStrat = rateStrat;
		this.currency = currency;
	}

	public void addPayment(int coinValue) throws IllegalCoinException {
			currency.insertCoin(coinValue);
			insertedSoFar += coinValue;
			timeBought = rateStrat.computeRate(insertedSoFar);
	}
	
	public void changeRateStrategy(RateStrategy pRateStrat) {
		rateStrat = pRateStrat;
		timeBought = rateStrat.computeRate(insertedSoFar);
	}

	public int readDisplay() {
		return timeBought;
	}

	public Receipt buy() {
		Receipt r = new ReceiptImpl(timeBought);
		reset();
		return r;
	}

	public void cancel() {
		reset();
	}

	private void reset() {
		timeBought = insertedSoFar = 0;
	}
	
	public int sumFromMToN(int m, int n) {
		int sum = 0;
		for (int i = 1; i<= n; i++) {
			sum =+ i;
		}
		for (int i = 1; i < m; i++) {
			sum -= i;
		}
		return sum;
	}
	
	public int sumfromMToN2(int m, int n) {
		int sum = 0;
		for (int i = m; i<= n; i++) {
			sum =+ i;
		}
		return sum;
	}
}
