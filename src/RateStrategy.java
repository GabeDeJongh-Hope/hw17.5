package src;

public interface RateStrategy {

	/**
	 * This method takes the time that the car has been parked then computes the appropriate amount due.
	 * @param time
	 * @return amount
	 */
	public int computeRate(int amount);
}
