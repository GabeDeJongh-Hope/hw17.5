package src;

public class CurrencyUSA implements CurrencyType {

	public void insertCoin(int coinValue) throws IllegalCoinException {
	    switch ( coinValue ) {
	    case 1: break;
	    case 5: break;
	    case 10: break;  
	    case 25: break;  
	    default: 
	      throw new IllegalCoinException("Invalid coin: "+coinValue);
	    }
	}
}
