	package test;

import org.junit.*;

import src.CurrencyDenmark;
import src.CurrencyType;
import src.CurrencyUSA;
import src.IllegalCoinException;
import src.PayStation;
import src.PayStationImpl;
import src.RateStrategy;
import src.RateStrategyAlpha;
import src.RateStrategyBeta;
import src.Receipt;
import src.ReceiptImpl;

import static org.junit.Assert.*;

/**
 * Testcases for the Pay Station system.
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Computer Science Department Aarhus University
 * 
 * This source code is provided WITHOUT ANY WARRANTY either expressed or
 * implied. You may study, use, modify, and distribute it for non-commercial
 * purposes. For any commercial use, see http://www.baerbak.com/
 */
public class IntegrationTests {
	PayStation ps;

	/** Fixture for pay station testing. */
	@Before
	public void setUp() {
		CurrencyType dollar = new CurrencyUSA();
		RateStrategy rs = new RateStrategyAlpha();
		ps = new PayStationImpl(rs, dollar);
	}

	/**
	 * Entering 5 cents should make the display report 2 minutes parking time.
	 */
	@Test
	public void shouldDisplay2MinFor5Cents() throws IllegalCoinException {
		ps.addPayment(5);
		assertEquals("Should display 2 min for 5 cents", 5 / 5 * 2, ps.readDisplay());
	}

	/**
	 * Entering 25 cents should make the display report 10 minutes parking time.
	 */
	@Test
	public void shouldDisplay10MinFor25Cents() throws IllegalCoinException {
		ps.addPayment(25);
		assertEquals("Should display 10 min for 25 cents", 25 / 5 * 2, ps.readDisplay());
		// 25 cent in 5 cent coins each giving 2 minutes parking
	}

	/**
	 * Entering 10 and 25 cents should be valid and return 14 minutes parking
	 */
	@Test
	public void shouldDisplay14MinFor10And25Cents() throws IllegalCoinException {
		ps.addPayment(10);
		ps.addPayment(25);
		assertEquals("Should display 14 min for 10+25 cents", (10 + 25) / 5 * 2, ps.readDisplay());
	}

	/**
	 * Buy should return a valid receipt of the proper amount of parking time
	 */
	@Test
	public void shouldReturnCorrectReceiptWhenBuy() throws IllegalCoinException {
		ps.addPayment(5);
		ps.addPayment(10);
		ps.addPayment(25);
		Receipt receipt;
		receipt = ps.buy();
		assertNotNull("Receipt reference cannot be null", receipt);
		assertEquals("Receipt value must be 16 min.", (5 + 10 + 25) / 5 * 2, receipt.value());
	}

	/**
	 * Receipts must be able to store parking time values
	 */
	@Test
	public void shouldStoreTimeInReceipt() {
		Receipt receipt = new ReceiptImpl(30);
		assertEquals("Receipt can store 30 minute value", 30, receipt.value());
	}

	/**
	 * Buy for 100 cents and verify the receipt
	 */
	@Test
	public void shouldReturnReceiptWhenBuy100c() throws IllegalCoinException {
		ps.addPayment(10);
		ps.addPayment(10);
		ps.addPayment(10);
		ps.addPayment(10);
		ps.addPayment(10);
		ps.addPayment(25);
		ps.addPayment(25);

		Receipt receipt;
		receipt = ps.buy();
		assertEquals((5 * 10 + 2 * 25) / 5 * 2, receipt.value());
	}

	/**
	 * Verify that the pay station is cleared after a buy scenario
	 */
	@Test
	public void shouldClearAfterBuy() throws IllegalCoinException {
		ps.addPayment(25);
		ps.buy(); // I do not care about the result
		// verify that the display reads 0
		assertEquals("Display should have been cleared", 0, ps.readDisplay());
		// verify that a following buy scenario behaves properly
		ps.addPayment(10);
		ps.addPayment(25);
		assertEquals("Next add payment should display correct time", (10 + 25) / 5 * 2, ps.readDisplay());
		Receipt r = ps.buy();
		assertEquals("Next buy should return valid receipt", (10 + 25) / 5 * 2, r.value());
		assertEquals("Again, display should be cleared", 0, ps.readDisplay());
	}

	/**
	 * Verify that cancel clears the pay station
	 */
	@Test
	public void shouldClearAfterCancel() throws IllegalCoinException {
		ps.addPayment(10);
		ps.cancel();
		assertEquals("Cancel should clear display", 0, ps.readDisplay());
		ps.addPayment(25);
		assertEquals("Insert after cancel should work", 25 / 5 * 2, ps.readDisplay());
	}

	/**
	 * Buy for 100 cents and verify the receipt
	 */
	@Test
	public void shouldReturnReceiptWhenBuy100cDanishCurrency() throws IllegalCoinException {
		ps = new PayStationImpl(new RateStrategyAlpha(), new CurrencyDenmark());
		ps.addPayment(20);
		ps.addPayment(20);
		ps.addPayment(20);
		ps.addPayment(20);
		ps.addPayment(2);
		ps.addPayment(2);
		ps.addPayment(2);
		ps.addPayment(2);
		ps.addPayment(2);
		ps.addPayment(10);
		Receipt receipt;
		receipt = ps.buy();
		assertEquals((5 * 10 + 2 * 25) / 5 * 2, receipt.value());
	}
	
	// Beta Town rate strategy
	
	/**
	 * Entering 5 cents should make the display report 5 minutes parking time.
	 */
	@Test
	public void shouldDisplay5MinFor5Cents() throws IllegalCoinException {
		ps.changeRateStrategy(new RateStrategyBeta());
		ps.addPayment(5);
		assertEquals("Should display 5 min for 5 cents", 5, ps.readDisplay());
	}
	
	// Changing rateStrategy while running shouldn't effect the state of the paystation
	
	/**
	 * Entering 5 cents should make the display report 5 minutes parking time.
	 */
	@Test
	public void shouldNotChangeStateWhenChangingStrategy() throws IllegalCoinException {
		ps.addPayment(5);
		assertEquals("Should display 2 min for 5 cents", 5 / 5 * 2, ps.readDisplay());
		// Change the rate strategy
		ps.changeRateStrategy(new RateStrategyBeta());
		assertEquals("Should display 5 min for 5 cents", 5, ps.readDisplay());
	}
}
