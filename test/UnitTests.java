package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import src.CurrencyDenmark;
import src.CurrencyType;
import src.CurrencyUSA;
import src.IllegalCoinException;
import src.PayStation;
import src.PayStationImpl;
import src.RateStrategy;
import src.RateStrategyAlpha;
import src.RateStrategyBeta;
import src.Receipt;
import src.ReceiptImpl;

public class UnitTests {
	PayStation ps;
	CurrencyUSA dollar;
	  @Before
	  public void setUp() {
		  dollar = new CurrencyUSA();
		  RateStrategy rs = new RateStrategyAlpha();
		  ps = new PayStationImpl(rs,dollar);
	  }
	  
		/**
		 * Receipts must be able to store parking time values
		 */
		@Test
		public void shouldStoreTimeInReceipt() {
			Receipt receipt = new ReceiptImpl(30);
			assertEquals("Receipt can store 30 minute value", 30, receipt.value());
		}
		
		/**
		 * Adding Danish Currencies to American Paystation shouldn't work
		 * 
		 * @throws IllegalCoinException
		 */
		@Test(expected = IllegalCoinException.class)
		public void shouldNotExceptDanishPayment() throws IllegalCoinException {
			ps.addPayment(2);
		}
		
		/**
		 * Adding American Currencies to Danish Paystation shouldn't work
		 * 
		 * @throws IllegalCoinException
		 */
		@Test(expected = IllegalCoinException.class)
		public void shouldNotExceptAmericanPayment() throws IllegalCoinException {
			ps = new PayStationImpl(new RateStrategyAlpha(), new CurrencyDenmark());
			ps.addPayment(25);
		}
		
		/**
		 * Verify that illegal coin values are rejected.
		 */
		@Test(expected = IllegalCoinException.class)
		public void shouldRejectIllegalCoin() throws IllegalCoinException {
			ps.addPayment(17);
		}
		
		/**
		 * Entering 5 cents should make the display report 5 minutes parking time.
		 */
		@Test
		public void shouldDisplay5MinFor5Cents() throws IllegalCoinException {
			RateStrategy rsb = new RateStrategyBeta();
			int amount = rsb.computeRate(5);
			assertEquals("Should display 5 min for 5 cents", 5, amount);
		}
}
